const moment = require('moment');

const isDateC = (value) => {
    if (!value) {
        return false;
    }

    const fecha = moment(value);

    if (fecha.isValid()) {
        return true;
    } else {
        return false;
    }
}

module.exports = { isDateC };
