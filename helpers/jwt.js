const jwt = require('jsonwebtoken');

//Recibr los parametros que seran parte del payload
const generarJWT = (uid, name) => {
    return new Promise((resolve, reject) => {
        const payload = {
            uid, name
        }

        // console.log(process.env.SECRET_JWT_SEED);

        jwt.sign(payload, process.env.SECRET_JWT_SEED, {
            expiresIn: '2h'
        }, (err, token) => {
            if (err) {
                console.log(err);
                reject('No se pudo generar el token');
            } else {
                resolve(token);
            }

        });
    });
}

module.exports = {
    generarJWT
}