const { request, response } = require('express');
const bcrypt = require('bcrypt');
const Usuario = require('../models/Usuario.model');
const { generarJWT } = require('../helpers/jwt');

const crearUsuario = async (req, res = response) => {
    const { email, password } = req.body;

    try {

        let usuario = await Usuario.findOne({ email });
        if (usuario) {
            res.status(400).json({
                ok: false,
                msg: 'Un usuario existe con ese correo',
            });
        }

        usuario = new Usuario(req.body);

        // Encriptar password
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        await usuario.save();

        // Generar JWT
        const token = await generarJWT(usuario.id, usuario.name);

        res.status(201).json({
            ok: true,
            msg: 'registro',
            token
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el administrador..',
        });
    }
};

const loginUsuario = async (req = request, res = response) => {
    const { email, password } = req.body;

    try {

        let usuario = await Usuario.findOne({ email });
        if (!usuario) {
            res.status(400).json({
                ok: false,
                msg: 'Este usuario no existe con ese correo',
            });
        }

        //Confirmar password
        const validPassword = bcrypt.compareSync(password, usuario.password);

        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Password incorrecto',
            })
        }

        // Generar JWT
        const token = await generarJWT(usuario.id, usuario.name);

        res.status(201).json({
            ok: true,
            uid: usuario.id,
            name: usuario.name,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el administrador..',
        });
    }

}

const revalidarToken = async (req = request, res = reponse) => {

    const uid = req.uid;
    const name = req.name;

    const token = await generarJWT(uid, name);

    res.json({
        ok: true,
        uid,
        name,
        token
    });
}


module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}