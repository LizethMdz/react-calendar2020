/**
 * Rutas de Events
 * host + /api/events
 */

const { Router } = require('express');

const { check } = require('express-validator');
// const { isDateC } = require('../helpers/isDate');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const {isDateC} = require('../helpers/isDate.js');

const { getEventos, crearEvento, actualizarEvento, eliminarEvento } = require('../controllers/events.controller');

const router = Router();

// Todas tienes que pasar por la validación del JWT
router.use(validarJWT);


// Obtener eventos
router.get('/', getEventos);

// Crear un nuevo evento
router.post(
    '/',
    [
        check('title', 'El titulo es obligatorio').not().isEmpty(),
        check('start', 'Fecha de inicio es obligatoria').custom(isDateC),
        check('end', 'Fecha de finalización es obligatoria').custom(isDateC),
        validarCampos
    ],
    crearEvento
);

// Actualizar Evento
router.put(
    '/:id',
    [
        check('title', 'El titulo es obligatorio').not().isEmpty(),
        check('start', 'Fecha de inicio es obligatoria').custom(isDateC),
        check('end', 'Fecha de finalización es obligatoria').custom(isDateC),
        validarCampos
    ],
    actualizarEvento
);

// Borrar evento
router.delete('/:id', eliminarEvento);

module.exports = router;
